# Concilium-discord-bot



## Concilium

Concilium is a bot that you can self-host and use on your server to share the documents of the Catholic Church's Councils. Documents from the Second Vatican Council are now suported, with more to come.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
GPLv3.
All rights to the Documents reserved to the Holy See in Vatican City.

## Project status
Alpha - Documents from the Second Vatican Council.
